import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:flutter/rendering.dart';  // from pub.dev

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Begin Again Meditation Supplies'),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  int counter = 0;
  double total = 0.00;
  List<Item> _catalog = [new Item('ITEM 1', 'https://tse3.mm.bing.net/th?id=OIF.b5RTUhs%2fXw67IijPQ00s7w&pid=Api&P=0&w=300&h=300', 15.99),
    new Item('ITEM 2', 'https://tse4.mm.bing.net/th?id=OIP.aeDhhNzLQmD1m-S7UCh2PgHaLH&pid=Api&P=0&w=300&h=300', 13.99),
    new Item('ITEM 3', 'https://tse3.mm.bing.net/th?id=OIP.dV6Ks8Lpt2wgsTEYguVc1AHaEb&pid=Api&P=0&w=285&h=171', 130.99),
    new Item('ITEM 4', 'https://tse1.mm.bing.net/th?id=OIP.EEutliFIOFJKtkfPNRayOgHaHa&pid=Api&P=0&w=300&h=300', 103.99),
    new Item('ITEM 5', 'https://tse3.mm.bing.net/th?id=OIP.JI_ZUQPjF5Y2edsn5bT3RAHaEK&pid=Api&P=0&w=327&h=185', 25.99),
    new Item('ITEM 6', 'https://tse2.mm.bing.net/th?id=OIP.V1LyakzsFXKNTbvdZ8OAJgAAAA&pid=Api&P=0&w=300&h=300', 80.99),
    new Item('ITEM 7', 'https://tse1.mm.bing.net/th?id=OIP.FvSf9fqtRyzmWWWq9DxC8gHaHa&pid=Api&P=0&w=300&h=300', 120.99),
    new Item('ITEM 8', 'https://tse2.mm.bing.net/th?id=OIP.w696eu5hsWA2HT8oXwGkXgAAAA&pid=Api&P=0&w=190&h=178', 105.99),
    new Item('ITEM 9', 'https://tse4.mm.bing.net/th?id=OIP.F_OXcKrmIFh7_cwvj-_dPQHaHY&pid=Api&P=0&w=300&h=300', 85.99),
    new Item('ITEM 10', 'https://tse2.mm.bing.net/th?id=OIP.RgXNqTVm1jMl9SI85cbkAwAAAA&pid=Api&P=0&w=267&h=179', 18.99),
    new Item('ITEM 11', 'https://tse1.mm.bing.net/th?id=OIP.eMi5fAt_w5NLxj5J8XQHJAHaEK&pid=Api&P=0&w=299&h=169', 70.99),
    new Item('ITEM 12', 'https://tse2.mm.bing.net/th?id=OIP.bJTLK3x7Rc8ITEJIFi0SyAHaE8&pid=Api&P=0&w=253&h=170', 130.99),
    new Item('ITEM 13', 'https://tse3.mm.bing.net/th?id=OIP.gwNfXw0gYNCMcmYhqpivegHaHa&pid=Api&P=0&w=300&h=300', 200.99),
    new Item('ITEM 14', 'https://tse4.mm.bing.net/th?id=OIP.6EhW0zbaMZZHth1hpCa_twAAAA&pid=Api&P=0&w=204&h=162', 29.99),
    new Item('ITEM 15', 'https://tse3.mm.bing.net/th?id=OIP.cfsoFlWJ8YxVQGt5awd4cwHaGR&pid=Api&P=0&w=206&h=175', 29.99),
    new Item('ITEM 16', 'https://tse1.mm.bing.net/th?id=OIP.efckb-gsUX0DFTwxAkE1eAHaGJ&pid=Api&P=0&w=198&h=165', 29.99),
    new Item('ITEM 17', 'https://tse4.mm.bing.net/th?id=OIP.FGD-ym-Q85v_2apcZ7tPeAHaHy&pid=Api&P=0&w=300&h=300', 8.99),
    new Item('ITEM 18', 'https://tse1.mm.bing.net/th?id=OIP.Rci6l7SOAwCN_GCJnwfBRAHaFj&pid=Api&P=0&w=244&h=184', 15.99),
    new Item('ITEM 19', 'https://tse2.mm.bing.net/th?id=OIP.5dmsjC8jFiqxBTDGZ4pJYAHaDi&pid=Api&P=0&w=393&h=189', 5.99),
    new Item('ITEM 20', 'https://tse4.mm.bing.net/th?id=OIP.g70BamkWEGW2R00grLLqiAHaE8&pid=Api&P=0&w=306&h=205', 7.99),
    new Item('ITEM 21', 'https://tse1.mm.bing.net/th?id=OIP._ysZpErRTenW5CSRX2uRUQHaHa&pid=Api&P=0&w=300&h=300', 10.99),
    new Item('ITEM 22', 'https://tse2.mm.bing.net/th?id=OIP.fbKJ12j2ouBn97AH6OlEmwHaGr&pid=Api&P=0&w=194&h=176', 14.99),
    new Item('ITEM 23', 'https://tse3.mm.bing.net/th?id=OIP.lN1UnRTTX_YQG6e0zC4vkgHaE8&pid=Api&P=0&w=284&h=190', 6.99),
    new Item('ITEM 24', 'https://tse4.mm.bing.net/th?id=OIP.r_pUeq8nQ7yfa9P8-6VcCgHaHa&pid=Api&P=0&w=300&h=300', 9.99),
    new Item('ITEM 25', 'https://tse1.mm.bing.net/th?id=OIP.FTt_mB668MoGHNdSORa6wAHaHa&pid=Api&P=0&w=300&h=300', 6.99),
    new Item('ITEM 26', 'https://tse2.mm.bing.net/th?id=OIP.-VbQ6trLNfCZIC4kK3rVCgHaDt&pid=Api&P=0&w=367&h=184', 5.50),
    new Item('ITEM 27', 'https://tse3.mm.bing.net/th?id=OIP.LU6EF4gXz0cL__qaCbGH4gHaGU&pid=Api&P=0&w=216&h=185', 4.43),
    new Item('ITEM 28', 'https://tse4.mm.bing.net/th?id=OIP.Hj_AgZu9Weh3BjDsmVUPaQHaHa&pid=Api&P=0&w=300&h=300', 18.99),
    new Item('ITEM 29', 'https://tse1.mm.bing.net/th?id=OIP.Vv05AR9VZH1rMNTejMVcmAHaJE&pid=Api&P=0&w=300&h=300', 8.99),
    new Item('ITEM 30', 'https://tse2.mm.bing.net/th?id=OIP.RlpfU-PNKEfg_-fh1lpaxAHaGw&pid=Api&P=0&w=178&h=164', 120.99),
    new Item('ITEM 31', 'https://tse1.mm.bing.net/th?id=OIP.2ocMmObVE-ZouM-wq-Ot4gHaHa&pid=Api&P=0&w=300&h=300', 45.99),
    new Item('ITEM 32', 'https://tse2.mm.bing.net/th?id=OIP.6pJ_TMyDn4iit-wTDFuNbAHaHa&pid=Api&P=0&w=300&h=300', 30.99),
    new Item('ITEM 33', 'https://tse1.mm.bing.net/th?id=OIP.Njx2f9aLdoML575p1-42lQHaHa&pid=Api&P=0&w=300&h=300', 17.65),
    new Item('ITEM 34', 'https://tse4.mm.bing.net/th?id=OIP.Zmg8mAx2yeBaJflZJ_LlxgHaIf&pid=Api&P=0&w=300&h=300', 15.99),
    new Item('ITEM 35', 'https://tse2.mm.bing.net/th?id=OIP.9-A1WI3mDMOeFqYy0WFbzwHaE7&pid=Api&P=0&w=250&h=167', 16.99),
    new Item('ITEM 36', 'https://tse1.mm.bing.net/th?id=OIP.P_5HaLjMsPJwzw4tUM_LfgHaFl&pid=Api&P=0&w=223&h=169', 13.99),
    new Item('ITEM 37', 'https://tse3.mm.bing.net/th?id=OIP.cxSepobwWlXLS2d4kjnCTAHaHa&pid=Api&P=0&w=300&h=300', 20.99),
    new Item('ITEM 38', 'https://tse1.mm.bing.net/th?id=OIP.aL6tYXP0dRzpMVOb5ShqdAHaHa&pid=Api&P=0&w=300&h=300', 25.99),
    new Item('ITEM 39', 'https://tse3.mm.bing.net/th?id=OIP.S_RMH7RrtZkglfkyY5-h0wHaHa&pid=Api&P=0&w=300&h=300', 15.99),
    new Item('ITEM 40', 'https://tse4.mm.bing.net/th?id=OIP.orbXyx8zAQzJmqkU-CkxigHaHa&pid=Api&P=0&w=300&h=300', 14.99),
    new Item('ITEM 41', 'https://tse3.mm.bing.net/th?id=OIP.SufXm4IFdYAY6LdPQlRMIQHaFj&pid=Api&P=0&w=220&h=166', 19.99),
    new Item('ITEM 42', 'https://tse2.mm.bing.net/th?id=OIP.iN-eFNhbN6QcM2OGkMv77gHaHa&pid=Api&P=0&w=300&h=300', 14.99),
    new Item('ITEM 43', 'https://tse1.mm.bing.net/th?id=OIP.SYvnJaUoUKRqvykcDWZBEQAAAA&pid=Api&P=0&w=300&h=300', 12.99),
    new Item('ITEM 44', 'https://tse1.mm.bing.net/th?id=OIP.KOnd5oixY7UsOPU0VgGGSwAAAA&pid=Api&P=0&w=300&h=300', 7.99),
  ];
  List<Item> _cart = <Item>[];



  Widget _buildTile(Item item) =>  FittedBox(
      child: Column(
        children: [
          SizedBox(height: 10,),
          Image.network(item.imageUrl, height: 100, width: 100,),
          SizedBox(height: 15,),
          Text("\$" + item.price.toString()),
          FilterChip(
              selectedColor: Colors.purple[300],
              backgroundColor: Colors.purple,
              selected: _cart.contains(item),
              labelStyle: TextStyle(fontStyle: FontStyle.italic),
              labelPadding: EdgeInsets.fromLTRB(3, -6, 3, -6),
              label: Text('Add to Cart', style: TextStyle(color: Colors.white, fontSize: 10),),
              onSelected: (bool value){
                setState((){
                  if(value){
                    _cart.add(item);
                    total += item.price.toDouble();
                    counter++;
                  }
                  else{
                    total -= item.price.toDouble();
                    counter--;
                    _cart.removeWhere((Item x){
                      return x == item;
                    });
                  }
                });
              }
          ),
        ],
      ));
    // checking
  Widget GetItemWidgets(List<Item> items)
  {
    return new GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 0.0,
      mainAxisSpacing: 2.0,
        children: items.map((item) => _buildTile(item)).toList()
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: Container(
            color: Colors.purple,
            child: ListView(
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/emptyness.jpg'),
                          fit: BoxFit.cover
                      ),
                  ),
                ),
                ListTile(
                  title: Text("Home", style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    ),
                  ),
                  onTap: (){
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: Text("About", style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  ),
                  onTap: (){
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AboutPage()));
                  },
                ),
                ListTile(
                  title: Text("Contact Us", style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  ),
                  onTap: (){
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ContactPage()));
                  },
                ),
              ],
            ),
          ),
        ),
        appBar: AppBar(
        title: Center(child: Text(widget.title, style: TextStyle(color: Colors.white, fontSize: 17),)),
        actions: <Widget>[
          PopupMenuButton(
            icon: Badge(   // from pub.dev
              showBadge: counter.toString() == '0' ? false : true,
              badgeContent: Text(counter.toString(), style: TextStyle(color: Colors.white),),
              badgeColor: Colors.purple[900],
              child: new Icon(Icons.shopping_cart,
              color: Colors.purple[1300],
          ),
            ),
            itemBuilder: (context) =>[
              PopupMenuItem(
                child:
                  Column(
                    children: [
                      for(var item in _cart)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.network(item.imageUrl, height: 50, width: 50),
                            Text("\$" + item.price.toString()),
                          ],
                        ),
                      Text("Total: \$" + total.toStringAsFixed(2)),
                      RaisedButton(
                        color: Colors.purple,
                        child: Text(
                          "Proceed to Purchase",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: (){},
                      ),
                    ],
                  ),
              ),
            ],

          ),
        ],
      ),
      body: GetItemWidgets(_catalog)
      );
  }
}
class Item {
  String id;
  String imageUrl;
  double price;
  Item(id, imageUrl, price){
    this.id = id;
    this.imageUrl = imageUrl;
    this.price = price;
  }
}

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Begin Again Meditation Supplies", style: TextStyle(color: Colors.white, fontSize: 17),)),
      ),
      body: Column(
        children: <Widget>[
          Image.asset('assets/AboutPage.jpg',width: 500,height: 225, fit: BoxFit.cover,),
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
            "Begin Again Meditation supplies is a one of a kind store where " +
            "you can find the best meditation items to further" +
            "your journey into your spiritual world. Our store features" +
            "the best books whether you're a beginner or more of an" +
            "advanced meditator. Our store also features all types of" +
            "meditation pose devices that cater to all different body" +
            "types. Compared to other shops our selection has very" +
            "good prices and offers a range of prices for item types." +
            "We believe that people of all budgets should be able to" +
            "meditate. We also offer a 30 day money back guarantee" +
            "for every item purchased. We believe you should be" +
            "able to try items before fully committing to them. Thanks" +
            "for using our shopping app and please rate your shopping" +
            "experience on the play store.",
            style: TextStyle(fontSize: 14), textAlign: TextAlign.left, softWrap: true,
            ),
          )
            ],
          )
      );
  }
}
class ContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Begin Again Meditation Supplies", style: TextStyle(color: Colors.white, fontSize: 16),)),
        ),
      body: Column(
        children: [
          Text(
          '''
          
  Contact Us
          ''',
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25), textAlign: TextAlign.center,
          ),
          Text(
            '''
            Company Email: BeginAgain@Gmail.com
            Support Email:    BeginAgainSupport@Gmail.com
            Company Line:   1 800 563 8292
            Support Line:      1 800 222 4786
            ''',
            style: TextStyle(fontSize: 15),
          ),

          Container(
            margin: EdgeInsets.all(3.0),
            child: TextField(
              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 2.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 2.0),
                ),
                hintText: 'From: Your Email',
              ),
            ),
          ),

          Container(
            margin: EdgeInsets.all(3.0),
            padding: EdgeInsets.only(bottom: 10.0),
            child: TextField(
              maxLines: 4,
              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 2.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 2.0),
                ),
                hintText: 'Message:',
              ),
            ),
          )
        ],
      ),
    );
  }
}